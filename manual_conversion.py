#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from pprint import pprint

inputs = "inputs/"
outputs = "outputs/"

csv_input = inputs + "<filename>.csv"

if 'once' in csv_input:
    csv_output_base = outputs + 'once-'
elif 'regular' in csv_input:
    csv_output_base = outputs + 'regular-'
else:
    csv_output_base = outputs + 'nonstandard-'

csv_countries = "resources/countrylist_merged.csv"

all_donations = []
orgs_donations = []
indiv_donations = []

# Process the resources
countries = {}
with open(csv_countries, 'r') as csv_input_file:
    reader = csv.DictReader(csv_input_file, delimiter=';')

    for row in reader:
        c_id = row['ir_country_name']
        countries[c_id] = row

# Fields that need no data transformation
basic_fields = [
    'Email',
    'Civility',
    'First-name',
    'Last-name',
    'Date',
    'Fiscal receipt number',
    'City',
    'Postcode',
    'Country',
    'Frequency',
    'Payment mode',
    'Payment type']


def parse_donation(donation):
    ret = {}
    for f in basic_fields:
        ret[f] = donation[f]

    if donation['Company']:
        ret['Company'] = donation['Company']

    if 'Address' in donation:
        address = donation['Address'].split(' \\n')

        if len(address) > 1:
            ret['Street-Address'] = address[0]
            ret['Supplemental-Address-1'] = address[1]
        else:
            ret['Street-Address'] = address[0]
            ret['Supplemental-Address-1'] = ""
    elif 'donator_address1' in donation:
        ret['Street-Address'] = donation['donator_address1']
        ret['Supplemental-Address-1'] = donation['donator_address2']


    if ret['Country']:
        c_id = ret['Country']
        ret['Country'] = countries[c_id]['civ_5_country_name']

    if donation['Amount']:
        amount = donation['Amount']
        ret['Amount'] = amount[:-2].replace(',', '.').replace(' ', '')

    if donation['Frequency'] == 'regular':
        ret['Payment mode'] = 'SEPA DD Recurring Transaction'
    elif donation['Payment mode'] == 'card':
        ret['Payment mode'] = 'Carte bancaire'

    ret['Campaign type'] = 'Contribution campagne'
    ret['source'] = 'iRaiser'

    return ret


with open(csv_input, 'r') as csv_input_file:
    reader = csv.DictReader(csv_input_file, delimiter=';')

    all_donations = []

    for row in reader:
        if row['Donation status'] in ['validated', 'active']:
            all_donations.append(parse_donation(row))

for d in all_donations:
    if 'Company' in d:
        orgs_donations.append(d)
    else:
        indiv_donations.append(d)


fieldnames = [
    'Civility',
    'First-name',
    'Last-name',
    'Email',
    'Street-Address',
    'Supplemental-Address-1',
    'Postcode',
    'City',
    'Country',
    'Amount',
    'Date',
    'Payment mode',
    'Payment type',
    'Frequency',
    'Campaign type',
    'Fiscal receipt number',
    'source'
]

with open(csv_output_base + "orgs.csv", 'w') as csv_output_orgs:
    writer_orgs = csv.DictWriter(
        csv_output_orgs,
        fieldnames=['Company'] + fieldnames)

    writer_orgs.writeheader()

    for row in orgs_donations:
        writer_orgs.writerow(row)

with open(csv_output_base + "indiv.csv", 'w') as csv_output_indiv:
    writer_indiv = csv.DictWriter(
        csv_output_indiv,
        fieldnames=fieldnames)

    writer_indiv.writeheader()

    for row in indiv_donations:
        writer_indiv.writerow(row)
