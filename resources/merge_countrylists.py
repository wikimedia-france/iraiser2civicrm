#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from pprint import pprint
from unidecode import unidecode

cl_iraiser = "countrylist_iraiser.csv"
cl_civicrm = "countrylist_civicrm.csv"
cl_output = "countrylist_output.csv"

cl_global = {}
not_found = []
matches = 0

with open(cl_iraiser, 'r') as csv_input_file:
    print("Parsing iRaiser country list")
    reader = csv.DictReader(csv_input_file, delimiter=';')

    for row in reader:
        key = unidecode(row['ir_country_name'].strip().lower())
        cl_global[key] = dict(row)

with open(cl_civicrm, 'r') as csv_input_file:
    print("Parsing CiviCRM country list")
    reader = csv.DictReader(csv_input_file, delimiter=';')

    for row in reader:
        key = unidecode(row['civ_country_name'].strip().lower())

        if key in cl_global:
            cl_global[key]['civ_country_name'] = row['civ_country_name']
            cl_global[key]['civ_country_id'] = row['civ_country_id']
            matches = matches + 1
        else:
            not_found.append(dict(row))

with open(cl_output, 'w') as csv_output:
    fieldnames = [
        'ir_country_code',
        'ir_country_name',
        'civ_country_name',
        'civ_country_id']

    writer = csv.DictWriter(
        csv_output,
        fieldnames=fieldnames,
        delimiter=';')
    writer.writeheader()

    for c, v in cl_global.items():
        writer.writerow(v)

    print("{}/{} countries matched, saved to {}".format(
        matches,
        len(cl_global),
        cl_output))

print("{} unidentified country codes:".format(len(not_found)))
pprint(not_found)
