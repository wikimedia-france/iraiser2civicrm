#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Variants for exports made through an expert search on iRaiser

import csv
from os import path
from datetime import datetime
from unidecode import unidecode

inputs = "inputs"
outputs = "outputs"
timestamp = datetime.now().strftime('%Y-%m-%dT%H%M')

csv_input = path.join(inputs, 'export.csv')
csv_output_base = 'output-{}-'.format(timestamp)

csv_countries = "resources/countrylist_merged.csv"

all_donations = []
orgs_donations = []
indiv_donations = []


def check_company(name, email):
    name = unidecode(name).lower()
    email = unidecode(email).lower()

    name_bl = ["wikimedia france", "wikimedia", "wikipedia"]
    email_bl = ["dons@wikimedia.fr", "info@wikimedia.fr"]

    if name not in name_bl and email not in email_bl:
        return True
    else:
        return False


# Process the resources
countries = {}
with open(csv_countries, 'r') as csv_input_file:
    reader = csv.DictReader(csv_input_file, delimiter=';')

    for row in reader:
        c_id = row['ir_country_name']
        countries[c_id] = row

# Fields that need no data transformation
basic_fields = [
    ('Email', 'donator_email'),
    ('Civility', 'donator_civility'),
    ('First-name', 'donator_firstname'),
    ('Last-name', 'donator_lastname'),
    ('Date', 'donation_creation_date'),
    ('Fiscal receipt number', 'fiscal_receipt_number'),
    ('City', 'donator_city'),
    ('Postcode', 'donator_postcode'),
    ('Country', 'donator_country'),
    ('Frequency', 'donation_frequency'),
    ('Payment mode', 'payment_mode'),
    ('Payment type', 'payment_type'),
    ('Donation number', 'donation_number')]


def parse_donation(donation):
    ret = {}
    for (f1, f2) in basic_fields:
        ret[f1] = donation[f2]

    if donation['donator_company']:
        if check_company(donation['donator_company'], donation['donator_email']):
            ret['Company'] = donation['donator_company']
            ret['Company_email'] = donation['donator_email']

    # address = donation['Address'].split(' \\n')

    if donation['donator_address1']:
        ret['Street-Address'] = donation['donator_address1']

    if donation['donator_address2']:
        ret['Supplemental-Address-1'] = donation['donator_address2']

    if ret['Country']:
        c_id = ret['Country']
        ret['Country'] = countries[c_id]['civ_5_country_name']

    if donation['donation_amount_with_currency']:
        # "1 000,73 €" -> "1000.73"
        sum = donation['donation_amount_with_currency'].replace(' ', '')
        ret['Amount'] = sum.replace(',', '.').replace('€', '')

    if donation['donation_frequency'] == 'regular':
        ret['Payment mode'] = 'SEPA DD Recurring Transaction'
    elif donation['payment_mode'] == 'card':
        ret['Payment mode'] = 'Carte bancaire'
    elif donation['payment_mode'] == 'check':
        ret['Payment mode'] = 'Chèque'
    elif donation['payment_mode'] == 'gban':
        ret['Payment mode'] = 'SEPA DD One-off Transaction'

    if ret['Payment type'] == 'PayPal':
        ret['Payment mode'] = 'Paypal'

    ret['Campaign type'] = 'Contribution campagne'
    ret['source'] = 'iRaiser'

    return ret


with open(csv_input, 'r') as csv_input_file:
    reader = csv.DictReader(csv_input_file, delimiter=';')

    all_donations = []

    for row in reader:
        if row['donation_status'] == 'validated':
            all_donations.append(parse_donation(row))

for d in all_donations:
    if 'Company' in d:
        orgs_donations.append(d)
    else:
        indiv_donations.append(d)


fieldnames = [
    'Civility',
    'First-name',
    'Last-name',
    'Email',
    'Street-Address',
    'Supplemental-Address-1',
    'Postcode',
    'City',
    'Country',
    'Amount',
    'Date',
    'Payment mode',
    'Payment type',
    'Frequency',
    'Campaign type',
    'Fiscal receipt number',
    'Donation number',
    'source'
]

output_file_orgs = path.join(outputs, csv_output_base + "orgs.csv")
with open(output_file_orgs, 'w') as csv_output_orgs:
    writer_orgs = csv.DictWriter(
        csv_output_orgs,
        fieldnames=['Company', 'Company_email'] + fieldnames)

    writer_orgs.writeheader()

    for row in orgs_donations:
        writer_orgs.writerow(row)

output_file_indiv = path.join(outputs, csv_output_base + "indiv.csv")
with open(output_file_indiv, 'w') as csv_output_indiv:
    writer_indiv = csv.DictWriter(
        csv_output_indiv,
        fieldnames=fieldnames)

    writer_indiv.writeheader()

    for row in indiv_donations:
        writer_indiv.writerow(row)
